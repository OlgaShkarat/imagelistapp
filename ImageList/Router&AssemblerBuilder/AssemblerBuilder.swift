//
//  AssemblerBuilder.swift
//  ImageList
//
//  Created by Admin on 24.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol RouterMainProtocol {
    var navigationController: UINavigationController? { get set }
    var assemblyBuilder: AssemblerBuilderProtocol? { get set }
}

protocol RouterProtocol {
    func showInitialViewController()
}

class Router: RouterProtocol, RouterMainProtocol{
    
    var navigationController: UINavigationController?
    var assemblyBuilder: AssemblerBuilderProtocol?
    
    init(navigationController: UINavigationController, assemblyBuilder: AssemblerBuilderProtocol) {
        self.navigationController = navigationController
        self.assemblyBuilder = assemblyBuilder
    }
    
    func showInitialViewController() {
        if let navigationController = navigationController {
            guard let mainViewController = assemblyBuilder?.createImageListModule(router: self) else { return }
            navigationController.viewControllers = [mainViewController]
        }
    }
}
