//
//  Router.swift
//  ImageList
//
//  Created by Admin on 24.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol AssemblerBuilderProtocol {
    func createImageListModule(router: RouterProtocol) -> UIViewController
}

class AssemblerModuleBuilder: AssemblerBuilderProtocol {
    
    func createImageListModule(router: RouterProtocol) -> UIViewController {
        let view = ImageListViewController()
        let networkService = NetworkService()
        let presenter = ImageListPresenter(view: view, networkService: networkService, router: router)
        view.presenter = presenter
        return view
    }
}
