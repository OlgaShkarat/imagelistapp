//
//  NetworkService.swift
//  ImageList
//
//  Created by Admin on 23.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    func fetchImages(completion: @escaping (Result<ImageList?,Error>) -> Void)
}

final class NetworkService: NetworkServiceProtocol {
    
    func fetchImages(completion: @escaping (Result<ImageList?, Error>) -> Void) {
        
        guard let url = URL(string: API.url + API.path) else { return }

        let baseURL = URL(string: API.url)
     
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else { return }
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let images = try jsonDecoder.decode(ImageList.self, from: data)
                
                var arrayWithImages = [String]()
                
                for image in images {
                    let stringImage = baseURL?.appendingPathComponent(image).absoluteString.removingPercentEncoding
                    guard let image = stringImage else { return }
                    arrayWithImages.append(image)
                }
                completion(.success(arrayWithImages))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
}
