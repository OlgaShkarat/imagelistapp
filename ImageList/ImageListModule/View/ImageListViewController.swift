//
//  ImageListViewController.swift
//  ImageList
//
//  Created by Admin on 23.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ImageListViewController: UIViewController {
    
    //MARK:- Properties
    var presenter: ImageListPresenterProtocol?
    var collectionView: UICollectionView!
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshCollectionView), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "ImageList"
        createCollectionView()
    }
    
    //MARK: - createCollectionView
    private func createCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.reuseIdentifier)
        collectionView.addSubview(refreshControl)
    }
    
    @objc func refreshCollectionView() {
        presenter?.getImages()
    }
}
//MARK: - UICollectionViewDataSource
extension ImageListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.imageList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.reuseIdentifier, for: indexPath) as! ImageCell
        let image = (presenter?.imageList?[indexPath.row])!
        
        cell.configureImageView(image: image)
        return cell
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension ImageListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)
    }
}
//MARK:- ImageListViewProtocol
extension ImageListViewController: ImageListViewProtocol {
    func showWithSuccess() {
        collectionView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func showWithError() {
        print("Error")
    }
}
