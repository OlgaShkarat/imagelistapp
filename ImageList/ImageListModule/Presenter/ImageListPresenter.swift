//
//  ImageListPresenter.swift
//  ImageList
//
//  Created by Admin on 24.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol ImageListViewProtocol: class {
    func showWithSuccess()
    func showWithError()
}

protocol ImageListPresenterProtocol: class {
    var imageList: ImageList? { get set }
    func getImages()
}

class ImageListPresenter: ImageListPresenterProtocol {
    
    weak var view: ImageListViewProtocol?
    var imageList: ImageList?
    let networkService: NetworkServiceProtocol
    let router: RouterProtocol
    
   init(view: ImageListViewProtocol, networkService: NetworkServiceProtocol, router: RouterProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        getImages()
    }
    
    func getImages() {
        self.networkService.fetchImages { [weak self] (result) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let images):
                    self.imageList = images
                    self.view?.showWithSuccess()
                case .failure(let error):
                    print(error.localizedDescription)
                    self.view?.showWithError()
                }
            }
        }
    }
}
