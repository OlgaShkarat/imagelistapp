//
//  ImageCell.swift
//  ImageList
//
//  Created by Admin on 23.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCell: UICollectionViewCell {
    
    static let reuseIdentifier = "ImageCell"
    
    let myImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        setupConstraints()
    }
    
    func configureImageView(image: String) {
        guard let imageURL = URL(string: image) else { return }
        myImageView.sd_setImage(with: imageURL, completed: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.layer.cornerRadius = 4
        self.containerView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        myImageView.image = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Setup Constraints
extension ImageCell {
    
    private func setupConstraints() {
        addSubview(containerView)
        containerView.addSubview(myImageView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: self.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            myImageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            myImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            myImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            myImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
}
